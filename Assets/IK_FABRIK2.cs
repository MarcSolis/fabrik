using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class IK_FABRIK2 : MonoBehaviour
{
    public Transform[] joints;
    public Transform target;

    private Vector3[] copy;
    private float[] distances;
    private bool done;
    private Vector3 originalBase;
    void Start()
    {
        distances = new float[joints.Length - 1];
        for (int i = 0; i < joints.Length - 1; i++)
            distances[i] = Vector3.Distance(joints[i].position, joints[i + 1].position);
        
        copy = new Vector3[joints.Length];
        originalBase = copy[0];
    }

    void Update()
    {
        // Copy the joints positions to work with
       for(int i = 0; i< joints.Length; i++)
                copy[i] = joints[i].position;

        done = Vector3.Distance(copy[copy.Length - 1], target.position) < 0.01f;

        if (!done)
        {
            float targetRootDist = Vector3.Distance(copy[0], target.position);

            // Update joint positions
            if (targetRootDist > distances.Sum())
            {
                // The target is unreachable
                Vector3 newTarg = (target.position - originalBase).normalized * distances.Sum();
                fabrik(newTarg);

            }
            else
            {
                // The target is reachable
                fabrik(target.position);
            }

            // Update original joint rotations
            for (int i = 0; i <= joints.Length - 2; i++)
            {
            

                joints[i].up = -(copy[i] - copy[i + 1]).normalized;
                joints[i].position = copy[i];

            }          
        }
    }

    void fabrik(Vector3 _target)
    {
        while (Vector3.Distance(copy[copy.Length - 1], _target) > 0.1f)
        {
            // STAGE 1: FORWARD REACHING
            copy[copy.Length - 1] = _target;
            for (int i = copy.Length - 2; i >= 0; i--)
            {
                copy[i] = copy[i + 1] + (copy[i] - copy[i + 1]).normalized * distances[i];
            }

            // STAGE 2: BACKWARD REACHING
            copy[0] = originalBase;
            for (int i = 0; i < copy.Length - 2; i++)
            {
                copy[i + 1] = copy[i] + (copy[i + 1] - copy[i]).normalized * distances[i];
            }
        }
    }

}
